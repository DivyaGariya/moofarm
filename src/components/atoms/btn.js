import React from 'react';
import {View, Text, StyleSheet, TouchableHighlight} from 'react-native';
import * as Typography from '../../utility/typography';
import {WINDOW_HEIGHT, WINDOW_WIDTH} from '../../utility/mixins';
import LinearGradient from 'react-native-linear-gradient';

export default ({btnLabel, onPress}) => {
  return (
    <TouchableHighlight style={styles.btnContainer} onPress={() => onPress()}>
      <LinearGradient
        colors={['#8db600', '#008000']}
        start={{x: 1, y: 1}}
        end={{x: 0, y: 0}}
        style={styles.btnStyle}>
        <Text style={styles.btnTxt}>{btnLabel}</Text>
      </LinearGradient>
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  btnContainer: {
    width: WINDOW_WIDTH * 0.9,
    borderRadius: 50,
  },
  btnStyle: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: WINDOW_HEIGHT * 0.02,
    borderRadius: 50,
  },
  btnTxt: {
    fontSize: Typography.FONT_SIZE_18,
    color: 'white',
  },
});
