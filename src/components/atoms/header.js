import React from 'react';
import {View, Text, StatusBar, Image,StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { WINDOW_HEIGHT,WINDOW_WIDTH } from "../../utility/mixins";
import styles from '../../styles/header';

export default ({img}) => {
  return (
    <View style={styles.headerContainer}>
    <LinearGradient
      colors={['#008000', '#8db600']}
      start={{x: 1, y: 0}}
      end={{x: 0, y: 0}}
      style={styles.subHeader}>
        <View style={styles.imgView}>
         <Image
          source={img}
          style={styles.img}
          resizeMode='contain'
        />
        </View>
    </LinearGradient>
    <LinearGradient 
      style={styles.pinkView}
      colors={['#ffffff', '#FFC0CB']}
      start={{x: 0, y: 0.2}} 
      end={{x: 1, y: 0.8}}
      locations={[0.5,0.6]}
    />
    </View>
  );
};
