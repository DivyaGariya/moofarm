import {StyleSheet} from 'react-native';
import {WINDOW_HEIGHT, WINDOW_WIDTH} from '../utility/mixins';
import * as Typography from '../utility/typography';

export default StyleSheet.create({
  headerContainer: {
    height: WINDOW_HEIGHT * 0.32,
  },
  subHeader: {
    height: WINDOW_HEIGHT * 0.32,
    width: WINDOW_HEIGHT * 0.3,
    alignSelf: 'center',
    borderBottomLeftRadius: WINDOW_HEIGHT * 0.32,
    borderBottomRightRadius: WINDOW_HEIGHT * 0.32,
    transform: [{scaleX: 2.4}],
    elevation: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  pinkView: {
    backgroundColor: 'red',
    height: WINDOW_HEIGHT * 0.32,
    width: WINDOW_HEIGHT * 0.3,
    alignSelf: 'center',
    borderBottomLeftRadius: WINDOW_HEIGHT * 0.32,
    borderBottomRightRadius: WINDOW_HEIGHT * 0.32,
    transform: [{scaleX: 2.4}],
    position: 'relative',
    top: -WINDOW_HEIGHT * 0.31,
  },
  imgView:{
    height:WINDOW_WIDTH*0.09,
    width:WINDOW_WIDTH*0.09,
    backgroundColor:'rgba(255,255,255,0.2)',
    justifyContent:'center',
    alignItems:'center',
    borderRadius:WINDOW_WIDTH*0.09/2,
    transform: [{scaleY: 2.2}],
  },
  img:{
    height:WINDOW_HEIGHT*0.03,
    width:WINDOW_WIDTH*0.15,
  }
});
