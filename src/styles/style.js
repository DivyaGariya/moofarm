import { StyleSheet } from 'react-native';
import { WINDOW_HEIGHT, WINDOW_WIDTH } from '../utility/mixins';

export default StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  bottomViewStyle: {
    position: 'absolute',
    borderWidth: 1,
    height: WINDOW_HEIGHT * 0.1,
    borderBottomLeftRadius: WINDOW_HEIGHT * 0.5,
    borderBottomRightRadius: WINDOW_HEIGHT * 0.5,
    borderRadius: WINDOW_HEIGHT * 0.2,
    width: WINDOW_WIDTH * 0.8,
    transform: [{scaleX: 1}],
    alignSelf: 'flex-end',
    backgroundColor: 'pink',
    bottom: 0,
    right: 0,
  },
  bodyContainer: {
    alignItems: 'center',
  },
  borderStyleHighLighted: {
    borderColor: '#03DAC6',
  },
  underlineStyleHighLighted: {
    borderColor: 'pink',
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: WINDOW_HEIGHT * 0.03,
  },
});
