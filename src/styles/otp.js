import {StyleSheet} from 'react-native';
import {WINDOW_HEIGHT, WINDOW_WIDTH} from '../utility/mixins';
import * as Typography from '../utility/typography';

export default StyleSheet.create({
    verifTxtHeading: {
        fontSize: Typography.FONT_SIZE_22,
        fontWeight: 'bold',
        paddingTop: WINDOW_HEIGHT * 0.1,
        paddingBottom: WINDOW_HEIGHT * 0.02,
      },
      subHeading: {
        fontSize: Typography.FONT_SIZE_14,
        fontWeight: '800',
        paddingBottom: WINDOW_HEIGHT * 0.01,
      },
      countryCode: {
        flex: 0.1,
        fontSize: Typography.FONT_SIZE_16,
      },
      underlineStyleBase: {
        width: WINDOW_WIDTH * 0.12,
        height: WINDOW_HEIGHT > 600 ? WINDOW_HEIGHT * 0.05 : WINDOW_HEIGHT * 0.07,
        borderWidth: 2,
        color: 'black',
        borderRadius: 5,
      },
      otpInputStyle: {
        width: '60%',
        height: WINDOW_HEIGHT * 0.1,
      },
})