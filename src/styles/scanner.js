import {StyleSheet} from 'react-native';
import {scaleSize,WINDOW_HEIGHT,WINDOW_WIDTH} from '../utility/mixins';

export default StyleSheet.create({
  parentView: {
    flex: 1,
  },
  headerView:{
    backgroundColor:'transparent',
    height:WINDOW_HEIGHT*0.10,
    position:'absolute',
    top:0,
    left:0,
    right:0
  },
   headerArrowContainer:{
    backgroundColor: 'white',
    height: WINDOW_HEIGHT * 0.04,
    width: WINDOW_HEIGHT * 0.04,
    borderRadius: (WINDOW_HEIGHT * 0.04) / 2,
    top: WINDOW_HEIGHT * 0.04,
    left: WINDOW_WIDTH * 0.02,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footerContainer:{
    flex:0.3,
    backgroundColor:'white'
  },
  footerTopRow:{
    flex: 1,
    borderBottomWidth: 0.2,
    flexDirection: 'row',
    borderColor: 'rgba(0,0,0,0.05)',
  },
  footerBottomRow:{
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  fTopLeftView:{
    flex: 0.5,
    backgroundColor: 'white',
    justifyContent: 'center',
    borderTopLeftRadius: 10,
    justifyContent: 'center',
    paddingLeft: WINDOW_WIDTH * 0.02,
  },
  fGalleryBtn:{
    flex: 0.3,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  fCameraView:{
    flex: 0.4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  fCameraOuterView:{
    borderWidth: 1,
    height: WINDOW_WIDTH * 0.14,
    width: WINDOW_WIDTH * 0.14,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: (WINDOW_WIDTH * 0.14) / 2,
    borderColor: 'green',
  },
  fCameraBtn:{
    height: WINDOW_WIDTH * 0.12,
    width: WINDOW_WIDTH * 0.12,
    backgroundColor: 'green',
    borderRadius: (WINDOW_WIDTH * 0.12) / 2,
  },
  fFlashContainer:{
    flex: 0.3,
    justifyContent: 'center',
    alignItems: 'center',
  }
});
