import {StyleSheet, Platform, StatusBar} from 'react-native';
import {scaleSize, WINDOW_HEIGHT, WINDOW_WIDTH} from '../utility/mixins';
import * as Typography from '../utility/typography';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0,
  },
  bodyContainer: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header:{
      paddingHorizontal: WINDOW_WIDTH * 0.05,
    flex:0.9
  },
  stepTxt: {
    fontSize: Typography.FONT_SIZE_14,
    color: 'green',
    fontWeight: 'bold',
  },
  reviewTxt: {
    fontSize: Typography.FONT_SIZE_20,
    fontWeight: 'bold',
    paddingTop: WINDOW_HEIGHT * 0.005,
  },
  descTxt: {
    fontSize: Typography.FONT_SIZE_12,
    color: 'gray',
    paddingTop: WINDOW_HEIGHT * 0.01,
  },
  imgStyle: {
    width: WINDOW_WIDTH * 0.80,
    height: WINDOW_HEIGHT * 0.25,
    marginTop: WINDOW_HEIGHT * 0.1,
    borderRadius: 3,
    borderWidth: 1,
    backgroundColor: 'white',
    alignSelf: 'center'
  },
  footer: {
    flexDirection: 'row',
    flex: 0.1,
    alignItems: 'center',
    paddingVertical: WINDOW_HEIGHT * 0.02,
    justifyContent: 'space-evenly',

  },
  reTakeContainer: {
    backgroundColor: 'rgba(0,0,0,0.2)',
    flex: 0.45,
    height: WINDOW_HEIGHT * 0.05,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  nextContainer: {
    backgroundColor: 'green',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  btnTxt: {
    fontSize: Typography.FONT_SIZE_14,
    color: 'white',
  },
});
