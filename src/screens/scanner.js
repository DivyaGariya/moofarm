import React, {useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Image,
  TouchableHighlight,
  StatusBar,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {RNCamera} from 'react-native-camera';
import BarcodeMask, {LayoutChangeEvent} from 'react-native-barcode-mask';
import styles from '../styles/scanner';
import CommonStyle from '../styles/displayImg';
import {scaleSize, WINDOW_HEIGHT, WINDOW_WIDTH} from '../utility/mixins';
import ImagePicker from 'react-native-image-crop-picker';

export default (props) => {
  const [state, setState] = useState({
    isLoading: false,
    isCameraVisible: true,
    isHide: true,
    isGoBackModalVisible: false,
    images: [],
    flashStatus: RNCamera.Constants.FlashMode.off,
    clickedImg: '',
    iHeight: 0,
    iWidth: 0,
    ix: 0,
    iy: 0,
  });

  const selectFile = () => {
    ImagePicker.openPicker({
      width: WINDOW_WIDTH * 0.8,
      height: 400,
      cropping: true,
    }).then((image) => {
      setState({
        ...state,
        clickedImg: image?.path,
      });
      props.navigation.navigate('DisplayImg', {
        imageData: image?.path,
      });
    });
  };

  //On Camera click method
  const clickPhoto = async () => {
    if (camera) {
      const options_photo = {
        quality: 0.5,
        base64: true,
      };
      const data = await camera.takePictureAsync(options_photo);
      ImagePicker.openCropper({
        path: data.uri,
        width: 300,
        height: 200,
        cropping: true,
      }).then((image) => {
        console.log(image);
        setState({
          ...state,
          clickedImg: image?.path,
        });
        props.navigation.navigate('DisplayImg', {
          imageData: image?.path,
        });
      });
    } else {
      return;
    }
  };

  const onLayoutMeasuredHandler = (LayoutChangeEvent) => {
    console.log('LAYOUT', LayoutChangeEvent.nativeEvent);
  };

  //method for flash light off or on
  const flashFun = () => {
    if (state.flashStatus === RNCamera.Constants.FlashMode.on) {
      setState({
        ...state,
        flashStatus: RNCamera.Constants.FlashMode.off,
      });
    } else {
      setState({
        ...state,
        flashStatus: RNCamera.Constants.FlashMode.on,
      });
    }
  };

  return (
    <SafeAreaView style={styles.parentView}>
      <StatusBar backgroundColor="black" />
      <RNCamera
        ref={(ref) => {
          camera = ref;
        }}
        type={'back'}
        captureAudio={false}
        style={{flex: 0.8}}
        flashMode={state.flashStatus}
        rectOfInterest={{x: 0, y: 0, width: 0.1, height: 0.1}}
        cameraViewDimensions={{width: 0.5, height: 0.5}}
        ratio="4:3"
        isCameraVisible={false}
        androidCameraPermissionOptions={{
          title: 'Permission to use camera',
          message: 'We need your permission to use your camera',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}>
        <BarcodeMask
          showAnimatedLine={false}
          width={WINDOW_WIDTH * 0.8}
          edgeWidth={'100%'}
          edgeHeight={'100%'}
          edgeBorderWidth={1}
          edgeRadius={3}
          onLayoutMeasured={onLayoutMeasuredHandler}
        />
      </RNCamera>
      <View style={styles.headerView}>
        <TouchableHighlight
          style={styles.headerArrowContainer}
          underlayColor={'rgb(180, 180, 180)'}
          onPress={() => props.navigation.goBack()}>
          <Icon name="ios-arrow-back" size={scaleSize(20)} />
        </TouchableHighlight>
      </View>
      <View style={styles.footerContainer}>
        <View style={styles.footerTopRow}>
          <View style={styles.fTopLeftView}>
            <Text style={CommonStyle.stepTxt}> Step 2 of 3 </Text>
            <Text style={CommonStyle.reviewTxt}> Scan DL Front </Text>
          </View>
          <View style={styles.fTopLeftView}>
            {state.clickedImg !== '' ? (
              <Image
                source={{
                  uri: state.clickedImg,
                }}
                style={{
                  height: WINDOW_HEIGHT * 0.06,
                  width: WINDOW_WIDTH * 0.2,
                  borderRadius: 5,
                }}
              />
            ) : null}
          </View>
        </View>
        <View style={styles.footerBottomRow}>
          {/* Gallery btn */}
          <TouchableHighlight
            underlayColor={'rgba(0,0,0,0.02)'}
            style={styles.fGalleryBtn}
            onPress={() => selectFile()}>
            <>
              <Icon name="ios-images" size={scaleSize(30)} />
              <Text>Gallery</Text>
            </>
          </TouchableHighlight>
          {/* camera btn */}
          <View style={styles.fCameraView}>
            <TouchableHighlight
              underlayColor={'rgba(0,0,0,0.02)'}
              style={styles.fCameraOuterView}
              onPress={() => clickPhoto()}>
              <View style={styles.fCameraBtn} />
            </TouchableHighlight>
          </View>
          {/* Flash light view */}
          <TouchableHighlight
            underlayColor={'rgba(0,0,0,0.02)'}
            style={styles.fFlashContainer}
            onPress={() => flashFun()}>
            <Icon
              name={state.flashStatus ? 'ios-flash' : 'ios-flash-off'}
              size={scaleSize(30)}
            />
          </TouchableHighlight>
        </View>
      </View>
    </SafeAreaView>
  );
};
