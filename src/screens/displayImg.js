import React, {Component} from 'react';
import {useEffect} from 'react';
import {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StatusBar,
  Platform,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {scaleSize} from '../utility/mixins';
import styles from '../styles/displayImg';

export default (props) => {
  const [state, setState] = useState({
    imageURL: '',
  });

  useEffect(() => {
    setState({
      ...state,
      imageURL: props?.route?.params?.imageData,
    });
  }, []);
  const backHandlerFun = () => {
    props.navigation.goBack();
  };

  //Retake the image
  const retakeFun = () => {
    setState(
      {
        ...state,
        imageURL: '',
      },
      props.navigation.navigate('Scanner'),
    );
  };

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="gray" />
      <Icon
        name="ios-arrow-back"
        size={scaleSize(30)}
        onPress={() => backHandlerFun()}
      />
      <View style={styles.bodyContainer}>
        <View style={styles.header}>
          <Text style={styles.stepTxt}>Step 2 of 3</Text>
          <Text style={styles.reviewTxt}>Review DL Front</Text>
          <Text style={styles.descTxt}>
            Before proceeding make sure that the data on the image is not
            blurred and entirely visible to you. Click 'retake' if you want to
            upload the image again.
          </Text>
          {state.imageURL !== '' ? (
            <Image
              style={styles.imgStyle}
              source={{uri: state.imageURL}}
              resizeMode="stretch"
            />
          ) : (
            <ActivityIndicator size="small" color="green" />
          )}
        </View>
        <View style={styles.footer}>
          <TouchableOpacity
            onPress={() => retakeFun()}
            style={styles.reTakeContainer}>
            <Text style={[styles.btnTxt, {color: 'green'}]}>Retake</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => alert('WIP')}
            style={[styles.reTakeContainer, {backgroundColor: 'green'}]}>
            <Text style={styles.btnTxt}>Next</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};
