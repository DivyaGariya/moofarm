import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  KeyboardAvoidingView,
  ScrollView,
  Alert,
} from 'react-native';
import OTPInputView from '@twotalltotems/react-native-otp-input';
//common Header,Button,styles
import Button from '../components/atoms/btn';
import Header from '../components/atoms/header';
import styles from '../styles/style';
//otp style
import otpStyle from '../styles/otp';

export default (props) => {
  //component state
  const [otp, setOTP] = useState();

  //method for auto OTP verify
  const verifyOTP = (code) => {
    console.log('otppppp',code)
    fetch(
      `https://2factor.in/API/V1/06cc6ea1-5555-11eb-8153-0200cd936042/SMS/VERIFY/${props.route.params.sID}/${otp}`,
      {
        method: 'GET',
      },
    )
      .then((response) => response.json())
      .then((res) => {
        console.log('OTP response', res);
        Alert.alert('', res.Details, [
          {
            text: 'Ok',
            onPress: () =>
              res.Status.match('Success')
                ? 
                  Alert.alert(
                    '',
                    'Your number is verified!',
                    [
                      {
                        text: 'OK',
                        onPress : () => props.navigation.navigate('Scanner')
                      }
                    ]
                  )
                : Alert.alert(
                  '',
                  'Number verification is failed',
                  [
                    {
                      text: 'OK',
                      onPress : () => console.log('Did not matched')
                    }
                  ]
                )
          },
        ]);
      })
      .catch((err) => {
        console.log(' response error', err);
      });
  };

  return (
    <KeyboardAvoidingView style={styles.container}>
      <ScrollView>
        <Header img ={require('../assets/e.png')}/>
        <View style={styles.bodyContainer}>
          <Text style={otpStyle.verifTxtHeading}>
            Phone Number Verification
          </Text>
          <Text style={otpStyle.subHeading}>
            Please enter your mobile number to
          </Text>
          <Text style={otpStyle.subHeading}>receive One Time Password</Text>
          <View style={styles.inputContainer}>
            <OTPInputView
              style={otpStyle.otpInputStyle}
              pinCount={4}
              code={otp}
              onCodeChanged={(code) => setOTP(code)}
              autoFocusOnLoad
              codeInputFieldStyle={otpStyle.underlineStyleBase}
              codeInputHighlightStyle={styles.underlineStyleHighLighted}
              onCodeFilled={(code) => console.log('code',code)}
            />
          </View>
          <Button btnLabel="Confirm" onPress={() => verifyOTP()} />
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

