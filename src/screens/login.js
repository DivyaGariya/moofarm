import React, {useState,useEffect} from 'react';
import {
  View,
  Text,
  KeyboardAvoidingView,
  ScrollView,
} from 'react-native';
import OTPInputView from '@twotalltotems/react-native-otp-input';
//common header,Button and style
import Header from '../components/atoms/header';
import Button from '../components/atoms/btn';
import styles from '../styles/style';
//login style
import logStyle from '../styles/login';

export default (props) => {
  //component states
  const [phNo, setPhNo] = useState();
  const [sID,setSID] = useState()

  // method to generate OTP
  const sendOTP = () => {
    if (phNo?.length !== 10) {
      alert('please enter your correct mobile no');
    } else {
      fetch(
        `https://2factor.in/API/V1/06cc6ea1-5555-11eb-8153-0200cd936042/SMS/+91${phNo}/${1234}`,
        {
          method: 'GET',
        },
      )
        .then((response) => response.json())
        .then((res) => {
          console.log('OTP response', res);
          if(res.Status.match('Success')){
            setSID(res?.Details);
            console.log('response log',res)
            props.navigation.navigate('OTP',{sID:res.Details});
          } 
        })
        .catch((err) => {
          console.log('response error', err);
        });
    }
  };
  return (
    <KeyboardAvoidingView style={styles.container}>
    <ScrollView >
        <Header img ={require('../assets/a.png')}/>
        <View style={styles.bodyContainer}>
          <Text style={logStyle.verifTxtHeading}>
            Phone Number Verification
          </Text>
          <Text style={logStyle.subHeading}>
            Please enter your mobile number to
          </Text>
          <Text style={logStyle.subHeading}>receive One Time Password</Text>
          <View style={styles.inputContainer}>
            <Text style={logStyle.countryCode}>+91</Text>
            <View style={{flex: 0.8}}>
              <OTPInputView
                style={logStyle.otpInputStyle}
                pinCount={10}
                code={phNo}
                onCodeChanged={(code) => setPhNo(code)}
                codeInputFieldStyle={logStyle.underlineStyleBase}
                codeInputHighlightStyle={styles.underlineStyleHighLighted}
                onCodeFilled={(code) =>console.log('code',code)}
              />
            </View>
          </View>
          <Button btnLabel="SEND OTP" onPress={() => sendOTP()} />
        </View>
    </ScrollView>
    </KeyboardAvoidingView>

  );
};
