import React from 'react';
import { View,Text,StatusBar } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from './src/screens/login';
import OTP from './src/screens/otp';
import Scanner from './src/screens/scanner';
import DisplayImg from './src/screens/displayImg';

const Stack = createStackNavigator();

export default () => {
  return(
    <>
    <StatusBar barStyle='light-content' translucent={true} backgroundColor='transparent'/>
    <NavigationContainer>
      <Stack.Navigator headerMode='none'>
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name='OTP' component={OTP}/>
        <Stack.Screen name="Scanner" component={Scanner} />
        <Stack.Screen name='DisplayImg' component={DisplayImg}/>
      </Stack.Navigator>
    </NavigationContainer>
    </>
  )
}